# project-statistics

This project show an example of gitlab-API usage in continuous integration for getting project statistics in a csv file. This version of the project is done by using the python gitlab API with a docker-in-docker approach. A python script is launched in a specific docker container that is loaded/recorded in the container registry of the project.

## First do enable your project CI, the shared runners (or custom runners) and the container registry.

Everything is explained [here](https://gitlab.inria.fr/gitlabci_gallery/intro#prerequisites).

## Create a project access token and declare it as a CI/CD variable

1. In `Settings - Access token`, create a new project access token with 'developper' as role and check 'api' box.
2. Copy the token somewhere.
3. In `Settings - CI/CD - Variables` create a new variable with `Key` set to `PROJECT_TOKEN` and `Value` equal to the copied token.

You can find more explaination about CI/CD variables [here](https://gitlab.inria.fr/gitlabci_gallery/intro#cicd-variables).

## The Dockerfile

We put the Dockerfile in a specific `docker` folder which is a good practice if you want to add other docker application in your project.

The Dockerfile is quite simple. We notice that we use an ubuntu 22.04 image that is strord in a local registry to avoid any pulling problem.

## The python script

The python script is based on the [python-gitlab library](https://python-gitlab.readthedocs.io/en/stable/index.html). It uses a specific attribute of the `Project` object which name is `additionalstatistics`. Its `get` operator returns a RESTObject containing the number of downloads per day in the past 30 days. We then use the csv library to parse the result into a csv file.

## The .gitlab-ci.yml file


We use the template provided by the [register-dockerfile project](https://gitlab.inria.fr/inria-ci/docker/-/tree/main/register-dockerfile) to build the container and manage the registry.

The `statistics` job is executed in the container. It launches the python script and put the resulting csv file in the artifacts.