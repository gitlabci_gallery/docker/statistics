import os, csv
import gitlab


gl = gitlab.Gitlab(url="https://"+os.getenv("CI_SERVER_HOST"), private_token=os.getenv("PROJECT_TOKEN"), api_version=4)
gl.auth()

project = gl.projects.get(os.getenv("CI_PROJECT_ID"), statistics=True)
statistics = project.additionalstatistics.get()
with open('statistics.csv', 'w') as f:
    w = csv.DictWriter(f, fieldnames=statistics.fetches['days'][0].keys())
    w.writeheader()
    w.writerows(statistics.fetches['days'])

